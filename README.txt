Simple Python script to pull analtyics and subscription data from APIC v10 
using the REST API. The requests library is required, it can be installed
via pip.

To use:
1. create a properties file based on the properties.sample file
2. run the apic_v10_usage_analyzer script to pull the data
Then you can either use the scription_report or usage_report to generate a 
CSV report

ex:
python apic_v10_usage_analyzer.py -p myprops.json
python usage_report.py -p myprops.json