import sys
import json
import getopt
import time
import csv
from pprint import pprint


def printUsage():
    print('%s -p <property_file>' % (sys.argv[0]))


def main(argv):
    propertyfile = ''
    try:
        opts, args = getopt.getopt(argv, "hp:", ["propertyfile="])
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit()
        elif opt in ("-p", "--propertyfile"):
            propertyfile = arg
    if (propertyfile == ''):
        printUsage()
        sys.exit()

    with open(propertyfile) as file:
        props = json.load(file)

    with open('%s/report.json' % props['dataDirectory']) as file:
        data = json.load(file)

    with open('%s/usage-report.csv' % props['dataDirectory'], 'w', newline='') as csvfile:
        rowwriter = csv.writer(csvfile, delimiter=',')
        rowwriter.writerow(['Product Name', 'Product Title', 'Product Version', 'Product State', 'Gateway Type', 'Plan Name', 'Plan Title', 'Plan Calls', 'Plan Last Call',
                           'API Name', 'API Title', 'API Version', 'API Calls', 'API Last Call', 'Consumer Org Name', 'Consumer Org Title', 'Application Name', 'Application Title', 'Subscription State', 'Application Calls', 'Application Last Call', 'URI', 'URI Calls', 'URI Last Call'])
        for data_key in data.keys():
            row = []
            (product_name, product_version, plan_name) = data_key.split(':')
            row.append(product_name)
            row.append(data[data_key].get('product_title', product_name))
            row.append(product_version)
            row.append(data[data_key].get('product_state', 'not published'))
            row.append(data[data_key].get('gateway_types', 'unknown'))
            row.append(plan_name)
            row.append(data[data_key].get('plan_title', plan_name))
            if ('analytics_data' in data[data_key]):
                row.append(data[data_key]['analytics_data']['calls'])
                row.append(data[data_key]['analytics_data']['last_call'])
            else:
                row.append('')
                row.append('')

            if 'apis' in data[data_key]:
                for api_key in data[data_key]['apis'].keys():
                    api = data[data_key]['apis'][api_key]
                    row_api = row[:]
                    row_api.append(api['api_name'])
                    row_api.append(api['api_title'])
                    row_api.append(api['api_version'])
                    if (('analytics_data' in data[data_key]) and (api_key in data[data_key]['analytics_data']['apis'])):
                        row_api.append(data[data_key]['analytics_data']['apis'][api_key]['calls'])
                        row_api.append(
                            data[data_key]['analytics_data']['apis'][api_key]['last_call'])
                    else:
                        row_api.append('')
                        row_api.append('')

                    sub_keys = data[data_key]['subscriptions'].keys()
                    if (len(sub_keys) == 0):
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        row_api.append('')
                        rowwriter.writerow(row_api)
                    else:
                        for sub_key in sub_keys:
                            row_sub = row_api[:]
                            row_sub.append(
                                data[data_key]['subscriptions'][sub_key]['consumer_org_name'])
                            row_sub.append(
                                data[data_key]['subscriptions'][sub_key]['consumer_org_title'])
                            row_sub.append(
                                data[data_key]['subscriptions'][sub_key]['app_name'])
                            row_sub.append(
                                data[data_key]['subscriptions'][sub_key]['app_title'])
                            row_sub.append(
                                data[data_key]['subscriptions'][sub_key]['state'])
                            if (('analytics_data' in data[data_key]) and (api_key in data[data_key]['analytics_data']['apis']) and (sub_key in data[data_key]['analytics_data']['apis'][api_key]['org_apps'])):
                                row_sub.append(
                                    data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['calls'])
                                row_sub.append(
                                    data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['last_call'])
                                for uri in data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['uris'].keys():
                                    row_uri = row_sub[:]
                                    row_uri.append(uri)
                                    row_uri.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['uris'][uri]['calls'])
                                    row_uri.append(data[data_key]['analytics_data']['apis']
                                                [api_key]['org_apps'][sub_key]['uris'][uri]['last_call'])
                                    rowwriter.writerow(row_uri)
                            else:
                                row_sub.append('')
                                row_sub.append('')
                                row_sub.append('')
                                row_sub.append('')
                                row_sub.append('')
                                rowwriter.writerow(row_sub)
            else:
                for api_key in data[data_key]['analytics_data']['apis'].keys():
                    (api_name, api_version) = api_key.split(':')
                    api = data[data_key]['analytics_data']['apis'][api_key]
                    row_api = row[:]
                    row_api.append(api_name)
                    row_api.append(api_name)
                    row_api.append(api_version)
                    row_api.append(data[data_key]['analytics_data']['apis'][api_key]['calls'])
                    row_api.append(data[data_key]['analytics_data']['apis'][api_key]['last_call'])

                    for sub_key in data[data_key]['analytics_data']['apis'][api_key]['org_apps'].keys():
                        row_sub = row_api[:]
                        row_sub.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['org_name'])
                        row_sub.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['org_title'])
                        row_sub.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['app_name'])
                        row_sub.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['app_name'])
                        row_sub.append("no current subscription")
                        row_sub.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['calls'])
                        row_sub.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['last_call'])
                        for uri in data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['uris'].keys():
                            row_uri = row_sub[:]
                            row_uri.append(uri)
                            row_uri.append(data[data_key]['analytics_data']['apis'][api_key]['org_apps'][sub_key]['uris'][uri]['calls'])
                            row_uri.append(data[data_key]['analytics_data']['apis']
                                        [api_key]['org_apps'][sub_key]['uris'][uri]['last_call'])
                            rowwriter.writerow(row_uri)

if __name__ == "__main__":
    main(sys.argv[1:])
