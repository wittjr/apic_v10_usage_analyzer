import requests
import re
import sys
import json
import getopt
from pprint import pprint
import time
import urllib3
import pathlib
from datetime import datetime
from datetime import timedelta


class ExpiredTokenError(Exception):
    pass


class OutdatedFile(Exception):
    pass


def printUsage():
    print('%s -p <property_file>' % (sys.argv[0]))


def fileUpdatedToday(filename):
    fname = pathlib.Path(filename)
    mday = datetime.fromtimestamp(fname.stat().st_mtime).date()
    today = datetime.today().date()
    if (mday != today):
        return False
    return True

# call the analytics service for events, recursively to get all the events


def getEvents(analytics_service, props, headers, day):
    page_after = None
    events = []
    while True:
        size = props['recordLimit'] if (props['recordLimit'] >= 10) else 10
        query = {
            'terminate_early': False,
            'size': size,
            'before': 'now-%dd' % day,
            'after': 'now-%dd' % (day + 1),
            'filters': [
                {
                    'field': 'request_method',
                    'value': 'OPTIONS',
                    'type': 'not',
                    'query': 'term'
                },
                {
                    'field': 'plan_id',
                    'value': '',
                    'type': 'not',
                    'query': 'term'
                }
            ],
            'includes': [
                '@timestamp',
                'api_id',
                'api_name',
                'api_version',
                'app_id',
                'app_name',
                'developer_org_id',
                'developer_org_name',
                'developer_org_title',
                'endpoint_url',
                'plan_id',
                'plan_name',
                'product_name',
                'product_id',
                'query_string',
                'request_method',
                'status_code',
                'uri_path',
                'resource_path'
            ]
        }
        # Add the page_after value if this is a recursive call
        if (page_after != None):
            query['page_after'] = page_after

        # In testing was seeing some unresponsive issues, likely just due to
        # undersized environment but adding a retry just in case
        tries = 0
        events_array = []
        page_end = False
        while True:
            r = requests.post('https://%s/analytics/catalogs/%s/%s/analytics-services/%s/events' %
                            (props['host'], props['org'], props['catalog'], analytics_service['id']), json=query, verify=False, headers=headers)
            if (r.status_code == 200):
                events_array = r.json()['events']
                page_after = r.json().get('page_after')
                page_end = r.json().get('page_end')
                break
            tries += 1
            time.sleep(5)
            if (tries == 20):
                print('Ran out of retries trying to get events')
                sys.exit(2)
        for event in events_array:
            events.append(event['event'])
        if page_end:
            break
    return events


def main(argv):
    urllib3.disable_warnings()
    propertyfile = ''
    try:
        opts, args = getopt.getopt(argv, "hp:", ["propertyfile="])
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit()
        elif opt in ("-p", "--propertyfile"):
            propertyfile = arg
    if (propertyfile == ''):
        printUsage()
        sys.exit()

    with open(propertyfile) as file:
        props = json.load(file)

    headers = {
        'accept': 'application/json',
        'content-type': 'application/json'
    }

    tokenFile = '%s/token.json' % props['dataDirectory']
    try:
        with open(tokenFile) as file:
            token = json.load(file)
        if token['expire_time'] <= time.time():
            raise ExpiredTokenError()
    except (FileNotFoundError, ExpiredTokenError):
        # Login and get a token
        token_time = time.time()
        r = requests.post('https://%s/api/token' % (props['host']), json={
            'username': props['username'],
            'password': props['password'],
            'realm': 'provider/%s' % (props['realm']),
            'client_id': props['client_id'],
            'client_secret': props['client_secret'],
            'grant_type': 'password'
        }, verify=False, headers=headers)

        if (r.status_code != 200):
            pprint(r.text)
            sys.exit(r.status_code)

        # Save the token for future use
        # Some enironments may support a refresh token, future option to
        # make use of that
        token = r.json()
        token['expire_time'] = token_time + token['expires_in']
        with open(tokenFile, 'w') as file:
            json.dump(token, file)

    headers['authorization'] = '%s %s' % (
        token['token_type'], token['access_token'])

    productFile = '%s/products.json' % props['dataDirectory']
    try:
        if (not fileUpdatedToday(productFile)):
            print('%s is outdated, will update' % productFile)
            raise OutdatedFile()
        with open(productFile) as file:
            products = json.load(file)
            print('%s file loaded' % productFile)
    except (FileNotFoundError, OutdatedFile):
        r = requests.get('https://%s/api/catalogs/%s/%s/products' %
                         (props['host'], props['org'], props['catalog']), verify=False, headers=headers)
        products = r.json()['results']
        for product in products:
            api_urls = product['api_urls']
            product['apis'] = []
            for api_url in api_urls:
                r = requests.get(api_url, verify=False, headers=headers)
                product['apis'].append(r.json())

        with open(productFile, 'w') as file:
            json.dump(products, file)

    subscriptionFile = '%s/subscriptions.json' % props['dataDirectory']
    try:
        if (not fileUpdatedToday(subscriptionFile)):
            print('%s is outdated, will update' % subscriptionFile)
            raise OutdatedFile()
        with open(subscriptionFile) as file:
            subscriptions = json.load(file)
            print('%s file loaded' % subscriptionFile)
    except (FileNotFoundError, OutdatedFile):
        r = requests.get('https://%s/api/catalogs/%s/%s/subscriptions' %
                         (props['host'], props['org'], props['catalog']), verify=False, headers=headers)
        subscriptions = r.json()['results']
        for subscription in subscriptions:
            r = requests.get(
                subscription['app_url'], verify=False, headers=headers)
            subscription['app'] = r.json()
            r = requests.get(
                subscription['consumer_org_url'], verify=False, headers=headers)
            subscription['consumer'] = r.json()

        with open(subscriptionFile, 'w') as file:
            json.dump(subscriptions, file)

    data = {}
    product_map = {}
    for product in products:
        product_apis = {}
        product_map.update({product['id']: '%s:%s' %
                           (product['name'], product['version'])})
        for api in product['apis']:
            new_api = {
                'api_version': api['version'],
                'api_title': api['title'],
                'api_name': api['name']
            }
            product_apis.update(
                {'%s:%s' % (api['name'], api['version']): new_api})
        for plan in product['plans']:
            key = '%s:%s:%s' % (
                product['name'], product['version'], plan['name'])
            if ('apis' in plan):
                plan_apis = {}
                for api in plan['apis']:
                    new_api = {
                        'api_version': api['version'],
                        'api_title': api['title'],
                        'api_name': api['name']
                    }
                    plan_apis.update(
                        {'%s:%s' % (api['name'], api['version']): new_api})
            else:
                plan_apis = product_apis.copy()
            data[key] = {
                'product_title': product['title'],
                'product_name': product['name'],
                'product_version': product['version'],
                'plan_title': plan['title'],
                'plan_name': plan['name'],
                'gateway_types': product['gateway_types'],
                'product_state': product['state'],
                'apis': plan_apis,
                'subscriptions': {}
            }

    for subscription in subscriptions:
        product_id = subscription['product_url'].split('/')[-1]
        key = '%s:%s' % (product_map[product_id], subscription['plan'])
        app = subscription['app']
        consumer = subscription['consumer']
        sub = {
            'app_id': app['id'],
            'app_title': app['title'],
            'app_name':  app['name'],
            'consumer_org_id': consumer['id'],
            'consumer_org_name': consumer['name'],
            'consumer_org_title': consumer['title'],
            'state': subscription['state']
        }
        data[key]['subscriptions'].update(
            {'%s:%s' % (consumer['id'], app['id']): sub})

    if (props['processAnalytics']):
        today = datetime.today()
        r = requests.get('https://%s/api/catalogs/%s/%s/analytics-services' %
                         (props['host'], props['org'], props['catalog']), verify=False, headers=headers)
        analytics_services = r.json()['results']
        start = 0 if props['includeToday'] else 1

        # compile regular expressions for checking URIs
        compiled_reg_ex = {}
        for reg_ex in props['uriPathsWithParameters'].keys():
            compiled_reg_ex[re.compile(
                reg_ex)] = props['uriPathsWithParameters'][reg_ex]

        # One day at a time
        for day in range(start, props['days']):
            day_date = (today - timedelta(days=day)).date()
            # One analytics service at a time
            for analytics_service in analytics_services:
                eventsFile = '%s/events-%s-%s.json' % (
                    props['dataDirectory'], analytics_service['name'], day_date)
                try:
                    with open(eventsFile) as file:
                        events = json.load(file)
                    print('%s file loaded' % eventsFile)
                except FileNotFoundError:
                    raw_events = getEvents(
                        analytics_service, props, headers, day)

                    events = {}
                    for raw_event in raw_events:
                        filtered_uri_path = raw_event['uri_path']
                        for path_reg_ex in compiled_reg_ex.keys():
                            if path_reg_ex.match(filtered_uri_path):
                                filtered_uri_path = compiled_reg_ex[path_reg_ex]
                                break

                        uri = '%s %s' % (
                            raw_event['request_method'], filtered_uri_path)
                        if (raw_event['query_string'] != '' and len(raw_event['query_string']) > 0):
                            parm_array = []
                            parms = raw_event['query_string'].split('&')
                            for parm in parms:
                                parm_array.append(parm.split('=')[0])
                            parm_array.sort()
                            uri += '?%s' % ('&'.join(parm_array))

                        api_key = '%s:%s' % (
                            raw_event['api_name'], raw_event['api_version'])
                        org_app_key = '%s:%s' % (
                            raw_event['developer_org_id'], raw_event['app_id'])
                        if (raw_event['plan_id'] not in events):
                            events.update({raw_event['plan_id']: {
                                'calls': 1,
                                'last_call': raw_event['@timestamp'],
                                'apis': {
                                    api_key: {
                                        'calls': 1,
                                        'last_call': raw_event['@timestamp'],
                                        'org_apps': {
                                            org_app_key: {
                                                'org_name': raw_event['developer_org_name'],
                                                'org_title': raw_event['developer_org_title'],
                                                'app_name': raw_event['app_name'],
                                                'calls': 1,
                                                'last_call': raw_event['@timestamp'],
                                                'uris': {
                                                    uri: {
                                                        'calls': 1,
                                                        'last_call': raw_event['@timestamp']
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }})
                        else:
                            events[raw_event['plan_id']]['calls'] += 1
                            if (api_key not in events[raw_event['plan_id']]['apis']):
                                events[raw_event['plan_id']]['apis'].update({
                                    api_key: {
                                        'calls': 1,
                                        'last_call': raw_event['@timestamp'],
                                        'org_apps': {
                                            org_app_key: {
                                                'org_name': raw_event['developer_org_name'],
                                                'org_title': raw_event['developer_org_title'],
                                                'app_name': raw_event['app_name'],
                                                'calls': 1,
                                                'last_call': raw_event['@timestamp'],
                                                'uris': {
                                                    uri: {
                                                        'calls': 1,
                                                        'last_call': raw_event['@timestamp']
                                                    }
                                                }
                                            }
                                        }
                                    }
                                })
                            else:
                                events[raw_event['plan_id']]['apis'][api_key]['calls'] += 1
                                if (org_app_key not in events[raw_event['plan_id']]['apis'][api_key]['org_apps']):
                                    events[raw_event['plan_id']
                                           ]['apis'][api_key]['org_apps'].update({
                                               org_app_key: {
                                                   'org_name': raw_event['developer_org_name'],
                                                   'org_title': raw_event['developer_org_title'],
                                                   'app_name': raw_event['app_name'],
                                                   'calls': 1,
                                                   'last_call': raw_event['@timestamp'],
                                                   'uris': {
                                                       uri: {
                                                           'calls': 1,
                                                           'last_call': raw_event['@timestamp']
                                                       }
                                                   }
                                               }
                                           })
                                else:
                                    events[raw_event['plan_id']
                                           ]['apis'][api_key]['org_apps'][org_app_key]['calls'] += 1
                                    if (uri not in events[raw_event['plan_id']]['apis'][api_key]['org_apps'][org_app_key]['uris']):
                                        events[raw_event['plan_id']
                                               ]['apis'][api_key]['org_apps'][org_app_key]['uris'].update({
                                                   uri: {
                                                       'calls': 1,
                                                       'last_call': raw_event['@timestamp']
                                                   }
                                               })
                                    else:
                                        events[raw_event['plan_id']
                                               ]['apis'][api_key]['org_apps'][org_app_key]['uris'][uri]['calls'] += 1

                    with open(eventsFile, 'w') as file:
                        json.dump(events, file)

                for event_key in events.keys():
                    if (event_key not in data):
                        data[event_key] = {}

                    if ('analytics_data' not in data[event_key]):
                        data[event_key]['analytics_data'] = events[event_key].copy()
                    else:
                        data[event_key]['analytics_data']['calls'] += events[event_key]['calls']
                        if ('apis' not in data[event_key]['analytics_data']):
                            data[event_key]['analytics_data']['apis'] = events[event_key]['apis'].copy()
                        else:
                            for api_key in events[event_key]['apis'].keys():
                                if (api_key not in data[event_key]['analytics_data']['apis']):
                                    data[event_key]['analytics_data']['apis'][api_key] = events[event_key]['apis'][api_key].copy()
                                else:
                                    data[event_key]['analytics_data']['apis'][api_key]['calls'] += events[event_key]['apis'][api_key]['calls']
                                    if ('org_apps' not in data[event_key]['analytics_data']['apis'][api_key]):
                                        data[event_key]['analytics_data']['apis'][api_key]['org_apps'] = events[event_key]['apis'][api_key]['org_apps'].copy()
                                    else:
                                        for org_app_key in events[event_key]['apis'][api_key]['org_apps'].keys():
                                            if (org_app_key not in data[event_key]['analytics_data']['apis'][api_key]['org_apps']):
                                                data[event_key]['analytics_data']['apis'][api_key]['org_apps'][org_app_key] = events[event_key]['apis'][api_key]['org_apps'][org_app_key].copy()
                                            else:
                                                data[event_key]['analytics_data']['apis'][api_key]['org_apps'][
                                                    org_app_key]['calls'] += events[event_key]['apis'][api_key]['org_apps'][org_app_key]['calls']
                                                if ('uris' not in data[event_key]['analytics_data']['apis'][api_key]['org_apps'][
                                                    org_app_key]):
                                                    data[event_key]['analytics_data']['apis'][api_key]['org_apps'][
                                                        org_app_key]['uris'] = events[event_key]['apis'][api_key]['org_apps'][org_app_key]['uris'].copy()
                                                else:
                                                    for uri_key in events[event_key]['apis'][api_key]['org_apps'][org_app_key]['uris'].keys():
                                                        if (uri_key not in data[event_key]['analytics_data']['apis'][api_key]['org_apps'][
                                                            org_app_key]['uris']):
                                                            data[event_key]['analytics_data']['apis'][api_key]['org_apps'][
                                                                org_app_key]['uris'][uri_key] = events[event_key]['apis'][api_key]['org_apps'][org_app_key]['uris'][uri_key].copy()
                                                        else:
                                                            data[event_key]['analytics_data']['apis'][api_key]['org_apps'][
                                                                org_app_key]['uris'][uri_key]['calls'] += events[event_key]['apis'][api_key]['org_apps'][org_app_key]['uris'][uri_key]['calls']

    with open('%s/report.json' % props['dataDirectory'], 'w') as file:
        json.dump(data, file)


if __name__ == "__main__":
    main(sys.argv[1:])
