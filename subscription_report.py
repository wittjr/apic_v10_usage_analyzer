import sys
import json
import getopt
import time
import csv
from pprint import pprint


def printUsage():
    print('%s -p <property_file>' % (sys.argv[0]))


def main(argv):
    propertyfile = ''
    try:
        opts, args = getopt.getopt(argv, "hp:", ["propertyfile="])
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit()
        elif opt in ("-p", "--propertyfile"):
            propertyfile = arg
    if (propertyfile == ''):
        printUsage()
        sys.exit()

    with open(propertyfile) as file:
        props = json.load(file)

    with open('%s/report.json' % props['dataDirectory']) as file:
        data = json.load(file)

    with open('%s/subscription-report.csv' % props['dataDirectory'], 'w', newline='') as csvfile:
        rowwriter = csv.writer(csvfile, delimiter=',')
        rowwriter.writerow(['Product', 'Product Title', 'Product Version', 'Gateway Type', 'Plan', 'Plan Title',
                           'API', 'API Title', 'API Version', 'Consumer Org', 'Application', 'Subscription State'])
        data_keys = data.keys()
        for data_key in data_keys:
            row = []
            if 'product_name' not in data[data_key]:
                # If the product_name isn't defined it's just analytics data from an non-existing/old product
                continue
            row.append(data[data_key]['product_name'])
            row.append(data[data_key]['product_title'])
            row.append(data[data_key]['product_version'])
            row.append(data[data_key]['gateway_types'])
            row.append(data[data_key]['plan_name'])
            row.append(data[data_key]['plan_title'])
            apis = data[data_key]['apis'].keys()
            subscriptions = data[data_key]['subscriptions'].keys()
            for api_key in apis:
                api = data[data_key]['apis'][api_key]
                row_api = row[:]
                row_api.append(api['api_name'])
                row_api.append(api['api_title'])
                row_api.append(api['api_version'])

                if (len(subscriptions) == 0):
                    row_api.append('')
                    row_api.append('')
                    row_api.append('')
                    rowwriter.writerow(row_api)
                else:
                    for subscription_key in subscriptions:
                        subscription = data[data_key]['subscriptions'][subscription_key]
                        row_sub = row_api[:]
                        row_sub.append(subscription['consumer_org_title'])
                        row_sub.append(subscription['app_title'])
                        row_sub.append(subscription['state'])
                        rowwriter.writerow(row_sub)


if __name__ == "__main__":
    main(sys.argv[1:])
